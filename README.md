## LoggingModule
Please refer to the [module wiki][] for all information regarding documentation, issue tracking and support.

### Contributing
Contributions in the form of pull requests are greatly appreciated.  Please add feature requests to our issue
tracker if you are unable to contribute yourself.  See the [module wiki][] for the issue tracker coordinates.

### Building from source
The source can be built using [Maven][] with JDK 8.

### License
Licensed under version 2.0 of the [Apache License][].

[module wiki]: https://foreach.atlassian.net/wiki/display/AX/LoggingModule
[Maven]: http://maven.apache.org
[Apache License]: http://www.apache.org/licenses/LICENSE-2.0